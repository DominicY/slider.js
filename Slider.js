﻿
"use strict";
function Slider (sliderElementID, buttonStartID, buttonStopID, buttonFasterID, buttonSlowerID, speedDisplayID) {
	var TIMERSPEED = 40;
    var MAXVALUE = 100;
    var MINVALUE = 0;
	// How much to speed up or slow down on Faster/Slower click
    var INCDELTA = 0.5;

	// Setting the HTML elements for displays and buttons
    var sliderElement = document.getElementById(sliderElementID);
    var buttonStartElement = document.getElementById(buttonStartID);
    var buttonStopElement = document.getElementById(buttonStopID);
    var buttonFasterElement = document.getElementById(buttonFasterID);
    var buttonSlowerElement = document.getElementById(buttonSlowerID);
    var speedDisplayElement = document.getElementById(speedDisplayID);

	// Whether to increment the slider or decrement it...
    this.increment = true;
	// Or stop it altogether
    this.stop = false;

	// One of JavaScript's more weird items... I think I'm using it correctly!
    var that = this;

    var timer = setInterval(function () {
        return that.TimerTick();
    }, TIMERSPEED);
	
	// Stores slider.value and writes this to its HTML element.
    this.SetValue = function (value) {
        this.value = value;
        sliderElement.value = value;
    }
	
	this.DisplaySpeed = function () {
        speedDisplayElement.innerText = "Speed: " + this.speed;
    }
    
	this.SetValue(Math.floor((Math.random() * 100) + 1));
    this.SetSpeed(Math.floor((Math.random() * 10) + 1));
	
    this.SetStartButton = function (buttonStartID) {
        buttonStartElement.addEventListener("click", BtnStartHandler);
    }
    
    this.SetStopButton = function (buttonStopID) {
        buttonStopElement.addEventListener("click", BtnStopHandler);
    }
	
	function BtnStartHandler(e) {
        // Needs 'that', since 'this' in this case is the HTML button element.
		that.Start();
    }

    function BtnStopHandler(e) {
		// Needs 'that', since 'this' in this case is the HTML button element.
        that.Stop();
    }
    
    this.Faster = function () {
        this.SetSpeed(this.speed + INCDELTA);
    }

    this.Slower = function () {
        if (this.speed > 0) {
            this.SetSpeed(this.speed - INCDELTA);
        }
    }
    
    this.SetFasterButton = function (buttonFasterID) {
        buttonFasterElement.addEventListener("click", BtnFasterHandler);
        //return buttonFasterID;
    }

    function BtnFasterHandler() {
		// Needs 'that', since 'this' in this case is the HTML button element.
        that.Faster();
    }

    this.SetSlowerButton = function (buttonSlowerID) {
        buttonSlowerElement.addEventListener("click", BtnSlowerHandler);
        //return buttonSlowerID;
    }

    function BtnSlowerHandler() {
		// Needs 'that', since 'this' in this case is the HTML button element.
        that.Slower();
    }

    this.IsFull = function () {
        if (this.value >= MAXVALUE) {
            return true;
        } else {
            return false;
        }
    }

    this.IsEmpty = function () {
        if (this.value <= MINVALUE) {
            return true;
        } else {
            return false;
        }
    }

    this.SetStartButton(buttonStartID);
    this.SetStopButton(buttonStopID);
    this.SetFasterButton(buttonFasterID);
    this.SetSlowerButton(buttonSlowerID);
	
	console.log(this);
};

Slider.prototype.DecrementValue = function () {
    this.value -= this.speed;
}

Slider.prototype.IncrementValue = function () {
    this.value += this.speed;
}

Slider.prototype.Start = function () {
    this.stop = false;
}

Slider.prototype.Stop = function () {
    this.stop = true;
}

Slider.prototype.SetSpeed = function (value) {
    this.speed = value;
	this.DisplaySpeed();
}

// Main update function
Slider.prototype.TimerTick = function () {
    if (this.IsFull()) {
        this.increment = false;
    }
    else if (this.IsEmpty()) {
        this.increment = true;
    }
    if (this.stop) {
        return;
    }
    if (this.increment) {
        this.IncrementValue();
    } else {
        this.DecrementValue();
    }
    this.SetValue(this.value);
}